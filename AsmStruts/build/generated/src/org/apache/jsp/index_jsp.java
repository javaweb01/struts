package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_iterator_value;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_property_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_s_iterator_value = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_property_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_s_iterator_value.release();
    _jspx_tagPool_s_property_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<link href=\"cssmenu/csmenu.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js' type='text/javascript'></script>\n");
      out.write("<script src=\"cssmenu/jsmenu.js\" type=\"text/javascript\"></script>\n");
      out.write("<link href='https://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>\n");
      out.write("<link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">\n");
      out.write("<div class=\"header\">\n");
      out.write("    <nav id=\"fixNav\" class=\"container\">\n");
      out.write("        <a class=\"fb\" href=\"#\"><i class=\"fa fa-home\" aria-hidden=\"true\"></i> </a>\n");
      out.write("\n");
      out.write("        <a class=\"fl\" href=\"#\"><i class=\"fa fa-bolt\" aria-hidden=\"true\"></i></a>\n");
      out.write("        <a class=\"tu\" href=\"#\"><i class=\"fa fa-tumblr\"></i></a>\n");
      out.write("        <a  id='SignIn' class=\"vi\" href=\"#\"><i class=\"fa fa-pencil\" aria-hidden=\"true\"></i></a>\n");
      out.write("    </nav>\n");
      out.write("    <div class=\"inner\">\n");
      out.write("        <div class=\"user-ui\">\n");
      out.write("            <div class=\"user-menu-toggle\">\n");
      out.write("                <div class=\"profile-img\" style=\"background-image:url(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(")\"></div>\n");
      out.write("                <span id=\"btn1\" class=\"simple-arrow fa fa-chevron-up\"></span>\n");
      out.write("            </div>\n");
      out.write("            <!-- User menu -->\n");
      out.write("            <div id='comment-editor' style='display:none !important' class=\"user-menu\">\n");
      out.write("                <div class=\"user-info\">\n");
      out.write("                    <div class=\"profile-img\"  style=\"background-image:url(");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write(")\"></div>\n");
      out.write("                    <h3 class=\"name\">");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${username}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("</h3>\n");
      out.write("                    <div class=\"ui btn normal\"> <a href=\"SignOut\" class=\"button delete\"><span>Sign Out</span></a>\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"menu-nav\">\n");
      out.write("                    <li></span>Personal page</li>\n");
      out.write("                    <li ><a id='ed' href=\"#\">Edit</a></li>\n");
      out.write("                    <li>My comment\n");
      out.write("                    </li>\n");
      out.write("                    <li></span>My post</li>\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("        </div>\n");
      out.write("    </div>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("</head>\n");
      out.write("<style>\n");
      out.write("    #fixNav{\n");
      out.write("\n");
      out.write("\n");
      out.write("        margin-left: 42%;\n");
      out.write("        float: left;\n");
      out.write("        margin-top: 10px;\n");
      out.write("        width: 300px;\n");
      out.write("    }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<link rel=\"stylesheet\" href=\"path/to/font-awesome/css/font-awesome.min.css\">\n");
      out.write("<link rel=\"stylesheet prefetch\" href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<style>\n");
      out.write("    .container {\n");
      out.write("        width: 100%;\n");
      out.write("        display: table;\n");
      out.write("        margin: 0;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a {\n");
      out.write("        display: table-cell;\n");
      out.write("        color: white;\n");
      out.write("        width: 14.2%;\n");
      out.write("        text-align: center;\n");
      out.write("        font-size: 2em;\n");
      out.write("        padding: .5em;\n");
      out.write("    }\n");
      out.write("    @media (max-width: 768px) {\n");
      out.write("        nav a {\n");
      out.write("            font-size: 1.5em;\n");
      out.write("        }\n");
      out.write("    }\n");
      out.write("    nav a:first-child {\n");
      out.write("        border-radius: 5px 0 0 5px;\n");
      out.write("    }\n");
      out.write("    @media (max-width: 768px) {\n");
      out.write("        nav a:first-child {\n");
      out.write("            border-radius: 0;\n");
      out.write("        }\n");
      out.write("    }\n");
      out.write("    nav a:last-child {\n");
      out.write("        border-radius: 0px 5px 5px 0px;\n");
      out.write("    }\n");
      out.write("    @media (max-width: 768px) {\n");
      out.write("        nav a:last-child {\n");
      out.write("            border-radius: 0;\n");
      out.write("        }\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a:first-child:nth-last-child(1) {\n");
      out.write("        width: 100%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* two items */\n");
      out.write("    nav a:first-child:nth-last-child(2),\n");
      out.write("    nav a:first-child:nth-last-child(2) ~ a {\n");
      out.write("        width: 50%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* three items */\n");
      out.write("    nav a:first-child:nth-last-child(3),\n");
      out.write("    nav a:first-child:nth-last-child(3) ~ a {\n");
      out.write("        width: 33.3333%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* four items */\n");
      out.write("    nav a:first-child:nth-last-child(4),\n");
      out.write("    nav a:first-child:nth-last-child(4) ~ a {\n");
      out.write("        width: 25%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* five items */\n");
      out.write("    nav a:first-child:nth-last-child(5),\n");
      out.write("    nav a:first-child:nth-last-child(5) ~ a {\n");
      out.write("        width: 20%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* six items */\n");
      out.write("    nav a:first-child:nth-last-child(6),\n");
      out.write("    nav a:first-child:nth-last-child(6) ~ a {\n");
      out.write("        width: 16.666%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* seven items */\n");
      out.write("    nav a:first-child:nth-last-child(7),\n");
      out.write("    nav a:first-child:nth-last-child(7) ~ a {\n");
      out.write("        width: 14.285%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /* eight items */\n");
      out.write("    nav a:first-child:nth-last-child(8),\n");
      out.write("    nav a:first-child:nth-last-child(8) ~ a {\n");
      out.write("        width: 12.5%;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    /*\n");
      out.write("    Assigning link background colors and setting the hover and active states. For quick adding of buttons, perform a search + replace on the 2 letter variable name.\n");
      out.write("    */\n");
      out.write("    nav a.fb {\n");
      out.write("        background: #3B5998;\n");
      out.write("        text-shadow: 1px 1px 0px #263961;\n");
      out.write("    }\n");
      out.write("    nav a.fb:hover {\n");
      out.write("        color: #3055a3 !important;\n");
      out.write("        background: #2d4373;\n");
      out.write("    }\n");
      out.write("    nav a.fb:active {\n");
      out.write("        color: #3B5998;\n");
      out.write("        background: #1e2e4f;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.tw {\n");
      out.write("        background: #55ACEE;\n");
      out.write("        text-shadow: 1px 1px 0px #1689e0;\n");
      out.write("    }\n");
      out.write("    nav a.tw:hover {\n");
      out.write("        color: #4cadf7 !important;\n");
      out.write("        background: #2795e9;\n");
      out.write("    }\n");
      out.write("    nav a.tw:active {\n");
      out.write("        color: #55ACEE;\n");
      out.write("        background: #147bc9;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.gp {\n");
      out.write("        background: #DD4B39;\n");
      out.write("        text-shadow: 1px 1px 0px #ac2d1e;\n");
      out.write("    }\n");
      out.write("    nav a.gp:hover {\n");
      out.write("        color: #e9422d !important;\n");
      out.write("        background: #c23321;\n");
      out.write("    }\n");
      out.write("    nav a.gp:active {\n");
      out.write("        color: #DD4B39;\n");
      out.write("        background: #96271a;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.ig {\n");
      out.write("        background: #517FA4;\n");
      out.write("        text-shadow: 1px 1px 0px #385771;\n");
      out.write("    }\n");
      out.write("    nav a.ig:hover {\n");
      out.write("        color: #4580b0 !important;\n");
      out.write("        background: #406582;\n");
      out.write("    }\n");
      out.write("    nav a.ig:active {\n");
      out.write("        color: #517FA4;\n");
      out.write("        background: #2f4a60;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.fl {\n");
      out.write("        background: #FF0084;\n");
      out.write("        text-shadow: 1px 1px 0px #b3005c;\n");
      out.write("    }\n");
      out.write("    nav a.fl:hover {\n");
      out.write("        color: #ff0084 !important;\n");
      out.write("        background: #cc006a;\n");
      out.write("    }\n");
      out.write("    nav a.fl:active {\n");
      out.write("        color: #FF0084;\n");
      out.write("        background: #99004f;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.tu {\n");
      out.write("        background: #32506D;\n");
      out.write("        text-shadow: 1px 1px 0px #1a2a39;\n");
      out.write("    }\n");
      out.write("    nav a.tu:hover {\n");
      out.write("        color: #2a5075 !important;\n");
      out.write("        background: #22364a;\n");
      out.write("    }\n");
      out.write("    nav a.tu:active {\n");
      out.write("        color: #32506D;\n");
      out.write("        background: #121d27;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("    nav a.vi {\n");
      out.write("        background: #00BF8F;\n");
      out.write("        text-shadow: 1px 1px 0px #007356;\n");
      out.write("    }\n");
      out.write("    nav a.vi:hover {\n");
      out.write("        color: #00bf8f !important;\n");
      out.write("        background: #008c69;\n");
      out.write("    }\n");
      out.write("    nav a.vi:active {\n");
      out.write("        color: #00BF8F;\n");
      out.write("        background: #005943;\n");
      out.write("    }\n");
      out.write("\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Using AJAX with JSp/Servlet with JSON return type</title>\n");
      out.write("\n");
      out.write("        <link href=\"csscomment/newcss.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("        <style>\n");
      out.write("            html, body {\n");
      out.write("                background: #41cac0;\n");
      out.write("                font-family: \"PT Sans\", \"Helvetica Neue\", \"Helvetica\", \"Roboto\", \"Arial\", sans-serif;\n");
      out.write("                color: #555f77;\n");
      out.write("                -webkit-font-smoothing: antialiased;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            input, textarea {\n");
      out.write("                outline: none;\n");
      out.write("                border: none;\n");
      out.write("                display: block;\n");
      out.write("                margin: 0;\n");
      out.write("                padding: 0;\n");
      out.write("                -webkit-font-smoothing: antialiased;\n");
      out.write("                font-family: \"PT Sans\", \"Helvetica Neue\", \"Helvetica\", \"Roboto\", \"Arial\", sans-serif;\n");
      out.write("                font-size: 1rem;\n");
      out.write("                color: #555f77;\n");
      out.write("            }\n");
      out.write("            input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {\n");
      out.write("                color: #ced2db;\n");
      out.write("            }\n");
      out.write("            input::-moz-placeholder, textarea::-moz-placeholder {\n");
      out.write("                color: #ced2db;\n");
      out.write("            }\n");
      out.write("            input:-moz-placeholder, textarea:-moz-placeholder {\n");
      out.write("                color: #ced2db;\n");
      out.write("            }\n");
      out.write("            input:-ms-input-placeholder, textarea:-ms-input-placeholder {\n");
      out.write("                color: #ced2db;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            p {\n");
      out.write("                line-height: 1.3125rem;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comments {\n");
      out.write("                margin: 2.5rem auto 0;\n");
      out.write("                max-width: 37rem;\n");
      out.write("                padding: 0 1.25rem;\n");
      out.write("                margin-left: 428px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comment-wrap {\n");
      out.write("                margin-bottom: 1.25rem;\n");
      out.write("                display: table;\n");
      out.write("                width: 100%;\n");
      out.write("                min-height: 5.3125rem;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .photo {\n");
      out.write("                padding-top: 0.625rem;\n");
      out.write("                display: table-cell;\n");
      out.write("                width: 3.5rem;\n");
      out.write("            }\n");
      out.write("            .photo .avatar {\n");
      out.write("                height: 2.25rem;\n");
      out.write("                width: 2.25rem;\n");
      out.write("                border-radius: 50%;\n");
      out.write("                background-size: cover;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comment-block {\n");
      out.write("                margin-top: 10px;\n");
      out.write("                margin-bottom: 20px;\n");
      out.write("                width: 100%;\n");
      out.write("                float: left;\n");
      out.write("                padding: 1rem;\n");
      out.write("                background-color: #fff;\n");
      out.write("                display: table-cell;\n");
      out.write("                vertical-align: top;\n");
      out.write("                border-radius: 0.1875rem;\n");
      out.write("                -webkit-box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);\n");
      out.write("                box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);\n");
      out.write("            }\n");
      out.write("            .comment-block textarea {\n");
      out.write("                width: 100%;\n");
      out.write("                resize: none;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comment-text {\n");
      out.write("                margin-bottom: 1.25rem;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .bottom-comment {\n");
      out.write("                color: #acb4c2;\n");
      out.write("                font-size: 0.875rem;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comment-date {\n");
      out.write("                float: left;\n");
      out.write("                margin-top: -21px;\n");
      out.write("                margin-left: 18px;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            .comment-actions {\n");
      out.write("                float: right;\n");
      out.write("            }\n");
      out.write("            .comment-actions li {\n");
      out.write("                display: inline;\n");
      out.write("                margin: -2px;\n");
      out.write("                cursor: pointer;\n");
      out.write("            }\n");
      out.write("            .comment-actions li.complain {\n");
      out.write("                padding-right: 0.75rem;\n");
      out.write("                border-right: 1px solid #e1e5eb;\n");
      out.write("            }\n");
      out.write("            .comment-actions li.reply {\n");
      out.write("                padding-left: 0.75rem;\n");
      out.write("                padding-right: 0.125rem;\n");
      out.write("            }\n");
      out.write("            .comment-actions li:hover {\n");
      out.write("                color: #0095ff;\n");
      out.write("            }\n");
      out.write("\n");
      out.write("        </style>\n");
      out.write("\n");
      out.write("        <script src=\"csscomment/ajax.js\" type=\"text/javascript\"></script>\n");
      out.write("        <script src=\"csscomment/jquery-3.3.1.min.js\" type=\"text/javascript\"></script>\n");
      out.write("\n");
      out.write("        <script type=\"text/javascript\">\n");
      out.write("\n");
      out.write("            function myFunction(obj) {\n");
      out.write("                var pro = $('#cm').val();\n");
      out.write("                var i_d = obj.id;\n");
      out.write("              \n");
      out.write("                $.ajax({\n");
      out.write("                    type: \"POST\",\n");
      out.write("                    data: {comment: pro, id: i_d},\n");
      out.write("                    url: \"comment\",\n");
      out.write("\n");
      out.write("                    success: function (responseJson) {\n");
      out.write("                      \n");
      out.write("                        var c = $('cc').height();\n");
      out.write("                        jQuery('html,body').animate({scrollTop: jQuery('.comment-block').offset().top}, c);\n");
      out.write("                      \n");
      out.write("                            var $dis = $('#cc');\n");
      out.write("                            var dem = 0;\n");
      out.write("                            var html = '<div  class=\"comment-block\">';\n");
      out.write("                           \n");
      out.write("                                 \n");
      out.write("                               \n");
      out.write("                                    html += '<h5>' + 123 + '</h5>';\n");
      out.write("                                \n");
      out.write("                               \n");
      out.write("  html += '<p class=\"comment-text\">' + (pro) + ' </div>';\n");
      out.write("\n");
      out.write("                                    html += '<div class=\"bottom-comment\">';\n");
      out.write("                                    html += '<div class=\"bottom-comment\">'\n");
      out.write("                                    html += '  <div class=\"comment-date\">' + 123 + '</div>';\n");
      out.write("\n");
      out.write("                                    html += \" </div></div></div>\";\n");
      out.write("                                    $('#cc').append(html);\n");
      out.write("                             \n");
      out.write("                                  \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                                \n");
      out.write("\n");
      out.write("    } \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                        \n");
      out.write("                    });\n");
      out.write("            }\n");
      out.write("\n");
      out.write("\n");
      out.write("        </script>\n");
      out.write("    </head>\n");
      out.write("    <body id=\"bd\">\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"district\">\n");
      out.write("            <p id=\"p1\"></p>  \n");
      out.write("            <p id=\"p2\"></p>  \n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <link href=\"csspost/newcss.css\" rel=\"stylesheet\" type=\"text/css\"/>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("       \n");
      out.write("\n");
      out.write("                ");
      if (_jspx_meth_s_iterator_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("  \n");
      out.write("            ");
      if (_jspx_meth_s_iterator_1(_jspx_page_context))
        return;
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("            <div id=\"footer\">  \n");
      out.write("\n");
      out.write("            </div><!--END #footer--> \n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("        <style>#footer {\n");
      out.write("\n");
      out.write("                bottom: 0;\n");
      out.write("\n");
      out.write("                /* Height of the footer */\n");
      out.write("                background:#6cf;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("\n");
      out.write("    </div>\n");
      out.write("\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_s_iterator_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_0 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_0.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_0.setParent(null);
    _jspx_th_s_iterator_0.setValue("post");
    int _jspx_eval_s_iterator_0 = _jspx_th_s_iterator_0.doStartTag();
    if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_0.doInitBody();
      }
      do {
        out.write("\n");
        out.write(" <div class=\"container\">\n");
        out.write("            <ul class=\"posts\">\n");
        out.write("                    <li class=\"post\">\n");
        out.write("                        <div class=\"post-content\">\n");
        out.write("\n");
        out.write("                            <img src=\"");
        if (_jspx_meth_s_property_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("\"/>\n");
        out.write("\n");
        out.write("\n");
        out.write("                            <footer> <p id=\"v\">\n");
        out.write("\n");
        out.write("                                    ");
        if (_jspx_meth_s_property_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("\n");
        out.write("\n");
        out.write("\n");
        out.write("                                </p>\n");
        out.write("                                <a href=\"https://kamilciesla.pl\"><i class=\"fa fa-heart\" aria-hidden=\"true\"></i></a>\n");
        out.write("                                <a href=\"Comment?id=");
        if (_jspx_meth_s_property_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("\"><i class=\"fa fa-comment\" aria-hidden=\"true\"></i></a>\n");
        out.write("                                <a href=\"https://twitter.com/@kamciesla\"><i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i></a></footer>\n");
        out.write("                        </div>\n");
        out.write("                    </li>\n");
        out.write("\n");
        out.write("                </ul>\n");
        out.write("\n");
        out.write("            </div>\n");
        out.write("                                <div id=\"cc\" class=\"comments\">\n");
        out.write("\n");
        out.write("                <div class=\"photo\">\n");
        out.write("                    <div class=\"avatar\" style=\"background-image: url('");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${avatar}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("')\"></div>\n");
        out.write("                </div>\n");
        out.write("                <div class=\"comment-block\">\n");
        out.write("                    <form action=\"\">\n");
        out.write("                        <textarea name=\"comment\" id=\"cm\" cols=\"30\" rows=\"3\" placeholder=\"Add comment...\"></textarea>\n");
        out.write("                    </form>\n");
        out.write("                </div>\n");
        out.write("\n");
        out.write("                <input id=\"");
        if (_jspx_meth_s_property_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_0, _jspx_page_context))
          return true;
        out.write("\" type = \"button\" value = \"Click Here\" onClick = \"myFunction(this)\">\n");
        out.write("          \n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_0);
      return true;
    }
    _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_0);
    return false;
  }

  private boolean _jspx_meth_s_property_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_0 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_0.setPageContext(_jspx_page_context);
    _jspx_th_s_property_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_0.setValue("post_image");
    int _jspx_eval_s_property_0 = _jspx_th_s_property_0.doStartTag();
    if (_jspx_th_s_property_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_0);
    return false;
  }

  private boolean _jspx_meth_s_property_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_1 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_1.setPageContext(_jspx_page_context);
    _jspx_th_s_property_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_1.setValue("post_content");
    int _jspx_eval_s_property_1 = _jspx_th_s_property_1.doStartTag();
    if (_jspx_th_s_property_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_1);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_1);
    return false;
  }

  private boolean _jspx_meth_s_property_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_2 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_2.setPageContext(_jspx_page_context);
    _jspx_th_s_property_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_2.setValue("post_id");
    int _jspx_eval_s_property_2 = _jspx_th_s_property_2.doStartTag();
    if (_jspx_th_s_property_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_2);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_2);
    return false;
  }

  private boolean _jspx_meth_s_property_3(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_3 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_3.setPageContext(_jspx_page_context);
    _jspx_th_s_property_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_0);
    _jspx_th_s_property_3.setValue("post_id");
    int _jspx_eval_s_property_3 = _jspx_th_s_property_3.doStartTag();
    if (_jspx_th_s_property_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_3);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_3);
    return false;
  }

  private boolean _jspx_meth_s_iterator_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:iterator
    org.apache.struts2.views.jsp.IteratorTag _jspx_th_s_iterator_1 = (org.apache.struts2.views.jsp.IteratorTag) _jspx_tagPool_s_iterator_value.get(org.apache.struts2.views.jsp.IteratorTag.class);
    _jspx_th_s_iterator_1.setPageContext(_jspx_page_context);
    _jspx_th_s_iterator_1.setParent(null);
    _jspx_th_s_iterator_1.setValue("comment");
    int _jspx_eval_s_iterator_1 = _jspx_th_s_iterator_1.doStartTag();
    if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_iterator_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_iterator_1.doInitBody();
      }
      do {
        out.write("\n");
        out.write("                <div class=\"photo\">\n");
        out.write("                    <div class=\"avatar\" style=\"background-image: url('");
        if (_jspx_meth_s_property_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_1, _jspx_page_context))
          return true;
        out.write("')\"></div>\n");
        out.write("                </div>\n");
        out.write("                <div  class=\"comment-block\">\n");
        out.write("\n");
        out.write("                    <h5>  ");
        if (_jspx_meth_s_property_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_1, _jspx_page_context))
          return true;
        out.write("</h5>\n");
        out.write("                    <div class=\"bottom-comment\">\n");
        out.write("                        <div class=\"bottom-comment\">\n");
        out.write("\n");
        out.write("                            <p class=\"comment-text\"> ");
        if (_jspx_meth_s_property_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_1, _jspx_page_context))
          return true;
        out.write("</div>\n");
        out.write("                        <div class=\"comment-date\">");
        if (_jspx_meth_s_property_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_iterator_1, _jspx_page_context))
          return true;
        out.write(" </div>\n");
        out.write("\n");
        out.write("\n");
        out.write("                    </div>\n");
        out.write("\n");
        out.write("                </div>\n");
        out.write("            ");
        int evalDoAfterBody = _jspx_th_s_iterator_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_iterator_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_iterator_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_1);
      return true;
    }
    _jspx_tagPool_s_iterator_value.reuse(_jspx_th_s_iterator_1);
    return false;
  }

  private boolean _jspx_meth_s_property_4(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_4 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_4.setPageContext(_jspx_page_context);
    _jspx_th_s_property_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_1);
    _jspx_th_s_property_4.setValue("cm_avatar");
    int _jspx_eval_s_property_4 = _jspx_th_s_property_4.doStartTag();
    if (_jspx_th_s_property_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_4);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_4);
    return false;
  }

  private boolean _jspx_meth_s_property_5(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_5 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_5.setPageContext(_jspx_page_context);
    _jspx_th_s_property_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_1);
    _jspx_th_s_property_5.setValue("cm_username");
    int _jspx_eval_s_property_5 = _jspx_th_s_property_5.doStartTag();
    if (_jspx_th_s_property_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_5);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_5);
    return false;
  }

  private boolean _jspx_meth_s_property_6(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_6 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_6.setPageContext(_jspx_page_context);
    _jspx_th_s_property_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_1);
    _jspx_th_s_property_6.setValue("cm_content");
    int _jspx_eval_s_property_6 = _jspx_th_s_property_6.doStartTag();
    if (_jspx_th_s_property_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_6);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_6);
    return false;
  }

  private boolean _jspx_meth_s_property_7(javax.servlet.jsp.tagext.JspTag _jspx_th_s_iterator_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:property
    org.apache.struts2.views.jsp.PropertyTag _jspx_th_s_property_7 = (org.apache.struts2.views.jsp.PropertyTag) _jspx_tagPool_s_property_value_nobody.get(org.apache.struts2.views.jsp.PropertyTag.class);
    _jspx_th_s_property_7.setPageContext(_jspx_page_context);
    _jspx_th_s_property_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_iterator_1);
    _jspx_th_s_property_7.setValue("cm_time");
    int _jspx_eval_s_property_7 = _jspx_th_s_property_7.doStartTag();
    if (_jspx_th_s_property_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_7);
      return true;
    }
    _jspx_tagPool_s_property_value_nobody.reuse(_jspx_th_s_property_7);
    return false;
  }
}
