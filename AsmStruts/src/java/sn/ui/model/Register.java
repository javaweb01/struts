/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sn.ui.model;

import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import sn.da.ProductDataAccess;

/**
 *
 * @author nam oio
 */
public class Register extends ActionSupport implements SessionAware {
    String username;
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public Register() {
    }
     public Map<String, Object> session;
    public String execute() throws Exception {
        ProductDataAccess da=new ProductDataAccess();
       if( da.createAccount(username, password, "")){
            session.put("userName", username);
                session.put("password", password);
                 return SUCCESS;
       }
   return ERROR;
    }
    @Override
    public void setSession(Map<String, Object> map) {
    this.session=map;  }
}
