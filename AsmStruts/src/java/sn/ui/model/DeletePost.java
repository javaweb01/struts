/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sn.ui.model;

import com.opensymphony.xwork2.ActionSupport;
import sn.da.ProductDataAccess;

/**
 *
 * @author nam oio
 */
public class DeletePost extends ActionSupport {
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public DeletePost() {
    }
    
    public String execute() throws Exception {
        
        new ProductDataAccess().deletePost(id);
       return SUCCESS;
    }
    
}
