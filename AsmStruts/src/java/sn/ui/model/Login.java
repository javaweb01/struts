/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sn.ui.model;

import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import sn.da.ProductDataAccess;
import sn.entity.Account;

/**
 *
 * @author nam oio
 */
public class Login extends ActionSupport implements SessionAware {
      private String username;
      private String password;
       private String signout;
     public Map<String, Object> session;
    public void setPassword(String password){
        this.password=password;
    }
    public void setUsername(String username){
        this.username=username;
    }
    
     public void setSignout(String signout){
        this.signout=signout;
    }
    public Login() {
    }
    
    public String execute() throws Exception {
        ProductDataAccess da=new ProductDataAccess();
       if(signout!=null){
          
           session.remove("userName");
           session.remove("password");
                 session.remove("avatar");
            return ERROR;
       }
      for(Account a: da.checkLogin(username, password)){
          if(a.getAc_username()!=null){
               session.put("userName", a.getAc_username());
                session.put("password", a.getAc_password());
                 session.put("avatar", a.getAc_avatar());
              return SUCCESS;
              
          }
      }
       return ERROR;
    }

    @Override
    public void setSession(Map<String, Object> map) {
    this.session=map;  }
    
}
