package sn.ui.model;


import com.opensymphony.xwork2.ActionContext;
import java.io.File;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import sn.da.ProductDataAccess;


public class PostUpload extends ActionSupport implements
        ServletRequestAware {

    
    private File postimg;
    private String postimgFileName;
     private String namepost;
private File file;

private String nameuser;

    public String getNameuser() {
        return nameuser;
    }

    public void setNameuser(String nameuser) {
        this.nameuser = nameuser;
    }
    
    public String getNamepost() {
        return namepost;
    }

    public void setNamepost(String namepost) {
        this.namepost = namepost;
    }
    
    private HttpServletRequest servletRequest;

    public String execute() {
       
         
      if(nameuser!=null){
          
          ProductDataAccess da=new ProductDataAccess();
        da.upProfile(nameuser, "images/"+postimgFileName);
      }else{
           Map session = ActionContext.getContext().getSession(); 

    String user=  (String) session.get("userName"); 
    
      ProductDataAccess da=new ProductDataAccess();
        da.setPost(user,namepost, "images/"+postimgFileName);
      }
          
            upFile(postimg, postimgFileName);
        
        return SUCCESS;
    }

    public void upFile(File file, String filename) {
        try {

            String filePath = servletRequest.getRealPath("/images");
            System.out.println("Server path:" + filePath);
            File fileToCreate = new File(filePath, filename);

            FileUtils.copyFile(file, fileToCreate);
        } catch (Exception e) {
            e.printStackTrace();
            addActionError(e.getMessage());

            //return INPUT;
        }
    }

 
   
    public void setPostimg(File postimg) {
        this.postimg = postimg;
    }

    

    public void setPostimgFileName(String postimgFileName) {
        this.postimgFileName = postimgFileName;
    }
    
    
    
    
    


  

    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;

    }
}
