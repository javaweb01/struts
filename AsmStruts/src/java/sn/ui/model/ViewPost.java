/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sn.ui.model;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import sn.da.ProductDataAccess;
import sn.entity.Comment;
import sn.entity.Product;
import sn.entity.post;

/**
 *
 * @author nam oio
 */
public class ViewPost extends ActionSupport {
     private List<Comment> comment;
      private List<post> post;
     private String id;
     public void setId(String id){
         this.id=id;
     }
    public ViewPost() {
    }
    
    public String execute() throws Exception {
         comment=new ProductDataAccess().getCmtId(id);
         post=new ProductDataAccess().getPostId(id);
         return SUCCESS;
    }
    public List<Comment> getComment(){
        return comment;
    }
     public List<post> getPost(){
        return post;
    }
}
